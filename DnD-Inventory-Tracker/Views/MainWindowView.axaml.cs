using System.ComponentModel;
using System.Threading.Tasks;
using Avalonia.Controls;
using Avalonia.ReactiveUI;
using DnD_Inventory_Tracker.ViewModels;
using DnD_Inventory_Tracker.Views;
using ReactiveUI;

namespace DnD_Inventory_Tracker.Views
{
    public partial class MainWindow : ReactiveWindow<MainWindowViewModel>
    {
        public MainWindow()
        {
            InitializeComponent();

            this.WhenActivated(d => d(ViewModel!.ShowDetailedView.RegisterHandler(DoShowDialogAsync)));
        }

        private async Task DoShowDialogAsync(InteractionContext<DetailedItemViewModel, ItemViewModel?> interaction)
        {
            var dialog = new DetailedItemView
            {
                DataContext = interaction.Input
            };

            var result = await dialog.ShowDialog<ItemViewModel>(this);
            interaction.SetOutput(result);
        }

        private void Window_OnClosing(object? sender, CancelEventArgs cancelEventArgs)
        {
            var vm = (MainWindowViewModel)DataContext!;
            vm.SaveData();
        }
    }
}
