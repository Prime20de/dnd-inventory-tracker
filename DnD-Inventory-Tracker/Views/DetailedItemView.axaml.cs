﻿using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using DnD_Inventory_Tracker.Models;

namespace DnD_Inventory_Tracker.Views;

public partial class DetailedItemView : Window
{
    public DetailedItemView()
    {
        InitializeComponent();
#if DEBUG
        this.AttachDevTools();
#endif
    }

    private void InitializeComponent()
    {
        AvaloniaXamlLoader.Load(this);
    }
}