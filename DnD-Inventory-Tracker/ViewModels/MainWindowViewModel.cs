﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Reactive;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Windows.Input;
using DnD_Inventory_Tracker.Models;
using DynamicData;
using ReactiveUI;

namespace DnD_Inventory_Tracker.ViewModels
{
    public class MainWindowViewModel : ViewModelBase
    {
        public MainWindowViewModel()
        {
            ShowDetailedView = new Interaction<DetailedItemViewModel, ItemViewModel?>();

            EditDetailedItem = ReactiveCommand.CreateFromTask(async () =>
            {
                var details = new DetailedItemViewModel();
                var result = await ShowDetailedView.Handle(details);
            });
            RxApp.MainThreadScheduler.Schedule(LoadData);
        }

        #region Data and Views

        public ICommand EditDetailedItem { get; }
        public Interaction<DetailedItemViewModel, ItemViewModel?> ShowDetailedView { get; }

        #endregion

        #region Private Properties and Indices

        // Initial values from MockData will be overwritten after loading the data
        private string _inventoryMaxWeight = "0";
        private List<Campaign>? _campaigns;
        private Campaign? _selectedCampaign;
        private Inventory? _selectedInventory;
        private List<Item>? _items;

        #endregion

        #region Binding variables

        public float CurrentWeight => SelectedInventory == null ? 0 : SelectedInventory.Items.Sum(item => item.Amount * item.Weight);
        public string InventoryMaxWeight
        {
            get => _inventoryMaxWeight;
            set => this.RaiseAndSetIfChanged(ref _inventoryMaxWeight, value);
        }

        public List<Campaign>? Campaigns
        {
            get => _campaigns;
            set => this.RaiseAndSetIfChanged(ref _campaigns, value);
        }
        public Campaign? SelectedCampaign
        {
            get => _selectedCampaign;
            set => this.RaiseAndSetIfChanged(ref _selectedCampaign, value);
        }
        public Inventory? SelectedInventory
        {
            get => _selectedInventory;
            set => this.RaiseAndSetIfChanged(ref _selectedInventory, value);
        }

        public List<Item>? Items
        {
            get => _items;
            set => this.RaiseAndSetIfChanged(ref _items, value);
        }

        #endregion

        public void SelectCampaign(Campaign campaign)
        {
            SelectedCampaign = campaign;
        }

        public void SelectInventory(Inventory inventory)
        {
            SelectedInventory = inventory;
            Items = inventory.Items;
        }

        public void DeleteGivenItem(Item item)
        {
            Debug.WriteLine("Item to be deleted: " + item.ItemName);
            Debug.WriteLine("Delete successfull: " + Items?.Remove(item));
        }

        public void CreateNewInventory(string? inventoryName)
        {
            if (string.IsNullOrEmpty(inventoryName) || SelectedCampaign == null) return;
            var inv = new Inventory(inventoryName, new List<Item>());
            SelectedCampaign.Inventories.Add(inv);
            Debug.WriteLine("New Inventory: " + inventoryName);
        }

        public void CreateNewCampaign(string? campaignName)
        {
            if (campaignName == null) return;
            var camp = new Campaign(campaignName, new List<Inventory>());
            Campaigns?.Add(camp);
            Debug.WriteLine("New Campaign: " + campaignName);
        }

        private async void LoadData()
        {
            var data = await DataHandler.LoadFromDisk();

            var campaigns = data.ToList();
            Campaigns = campaigns;
        }

        public async void SaveData()
        {
            if (Campaigns != null) await DataHandler.SaveDataAsync(Campaigns);
        }
    }
}
