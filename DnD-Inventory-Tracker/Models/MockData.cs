﻿using System.Collections.Generic;
using System.Text.Json;

namespace DnD_Inventory_Tracker.Models;

public static class MockData
{
    public static List<Item> itemMock =new List<Item>()
    {
        new Item("Greataxe", 1, 7, "Weapon"),
        new Item("Javelin", 15, 4, "Weapon"),
        new Item("Halskette", 1, 0, "Accessory")
    };

    public static List<Inventory> inventoryMock = new List<Inventory>() {new("Torterra", itemMock, "400")};
    public static Campaign campaignMock = new("K1", inventoryMock);
    public static string CampaignMockJson = JsonSerializer.Serialize(campaignMock);
}
