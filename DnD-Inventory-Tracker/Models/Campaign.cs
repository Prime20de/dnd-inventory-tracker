﻿using System.Collections.Generic;

namespace DnD_Inventory_Tracker.Models;

public class Campaign
{
    public string CampaignName { get; set; }
    public List<Inventory> Inventories { get; set; }

    public Campaign(string campaignName, List<Inventory> inventories)
    {
        this.CampaignName = campaignName;
        this.Inventories = inventories;
    }
}
