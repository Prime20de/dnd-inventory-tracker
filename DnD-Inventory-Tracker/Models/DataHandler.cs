﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace DnD_Inventory_Tracker.Models;

public static class DataHandler
{
    private const string DataLocationDirectory = "./Cache";
    private const string DataFile = DataLocationDirectory + "/Data";
    public static async Task SaveDataAsync(List<Campaign> data)
    {
        VerifyDataLocation();

        var jsonData = JsonSerializer.Serialize(data);
        await File.WriteAllTextAsync(DataFile, jsonData);
    }

    public static async Task<IEnumerable<Campaign>> LoadFromDisk()
    {
        VerifyDataLocation();

        await using var fs = File.OpenRead(DataFile);
        var file = await JsonSerializer.DeserializeAsync<Campaign[]>(fs).ConfigureAwait(false) ?? new []{MockData.campaignMock};

        return file.ToList();
    }

    private static void VerifyDataLocation()
    {
        if (!Directory.Exists(DataLocationDirectory))
        {
            Directory.CreateDirectory(DataLocationDirectory);
        }

        if (!File.Exists(DataFile))
        {
            File.Create(DataFile);
        }
    }
}
