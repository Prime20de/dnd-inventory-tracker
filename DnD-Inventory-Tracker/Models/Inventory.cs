﻿using System.Collections.Generic;

namespace DnD_Inventory_Tracker.Models;

public class Inventory
{
    public string InventoryName { get; set; }
    public List<Item> Items { get; set; }
    public string MaxWeight { get; set; }

    public Inventory(string inventoryName, List<Item> items, string maxWeight = "")
    {
        this.InventoryName = inventoryName;
        this.Items = items;
        this.MaxWeight = maxWeight;
    }
}
