﻿using System.Collections.ObjectModel;

namespace DnD_Inventory_Tracker.Models;

public class Item
{
    public string ItemName { get; set; }
    public int Amount { get; set; }
    public float Weight { get; set; }
    public string Category { get; set; }

    public Item(string itemName, int amount, float weight, string category)
    {
        this.ItemName = itemName;
        this.Amount = amount;
        this.Weight = weight;
        this.Category = category;
    }
}
